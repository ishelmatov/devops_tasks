#!/bin/bash

while true; do

	#Creating log entry
	current_date=$(date)
	mfile=$(find /tmp/test/files -type f -mtime -7 -exec ls -ahl {} + | awk 'NR==1{print}')
	echo "$current_date $mfile" >> /tmp/test/script-log/sync.log

	#Archiving the last modified file
	last_file=$(echo "$mfile"| awk '{print$NF}' | xargs basename)
	archive_name={$current_date}_{$last_file}
	tar -czvf /tmp/test/scipt-log/"$archive_name".tar.gz /tmp/test/files

	#Moving an archive into a destination path
	dest_dir="/tmp/test/archives"
	mv "/tmp/test/script-log/$archive_name.tar.gz" "$dest_dir"

	#Removing the source file
	file_path=$(echo "$mfile" | awk '{for (i=9; i<=NF; i++) printf "%s ", $i}')
	rm "$file_path"

	# Sleep for 15 minutes
	sleep 900
done

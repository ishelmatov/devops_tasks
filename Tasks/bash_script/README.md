1. Для нахождения файлов, которые были изменены за последние 7 дней мы используем команду 'mtime',
далее передаем вывод команды find оператору awk чтобы выбрать первый из списка. Далее добавляем строку в файл
sync.log с помощью оператора >> (> чтобы перезаписать файл полностью, а не добавить).

2. Чтобы архивировать исходный файл создаем переменную archive_name которой присваиваем текущую дату, время и 
название файла из прошлого шага.

3. Перемещаем архив командой mv предварительно определив путь.

4. Чтобы удалить исходный файл определим путь взяв строку из 1 шага и отфильтровав её оператором awk. Удаляем 
командой rm.

5. Оборачиваем весь скрипт в условный оператор while указав интервал между повторами 900 секунд (15 минут).

6. Запускаем скрипт командой 'nohup ./fetch_log.sh &' для повторения скрипта каждые 15 минут вне зависимости 
от того открыт терминал или нет. Логи скрипта можно посмотреть командой 'tail -f nohup.out', остановить скрипт
командой 'pkill -P <PID>'.